/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.menuoption;

import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Reference;
import com.affymetrix.common.PreferenceUtils;
import com.affymetrix.genometry.GenomeVersion;
import com.affymetrix.genometry.general.DataContainer;
import com.affymetrix.genometry.general.DataSet;
import com.affymetrix.genometry.style.ITrackStyleExtended;
import com.affymetrix.igb.shared.OpenURIAction;
import java.awt.Color;
import java.awt.FileDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import javax.swing.JOptionPane;
import org.lorainelab.igb.menu.api.MenuBarEntryProvider;
import org.lorainelab.igb.menu.api.model.MenuBarParentMenu;
import org.lorainelab.igb.menu.api.model.MenuItem;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFileChooser;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.lang3.StringUtils;
import org.lorainelab.igb.menu.api.model.MenuIcon;
import org.lorainelab.igb.services.IgbService;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author noorzahara
 */
@Component(immediate = true)
public class CreateQuickloadAction extends OpenURIAction implements MenuBarEntryProvider {

    private static final Logger LOG = LoggerFactory.getLogger(CreateQuickloadAction.class);
    private static final long serialVersionUID = 1L;
    private static final int MENU_POSITION = 7;
    private IgbService service;
    final public static boolean IS_MAC = System.getProperty("os.name").toLowerCase().contains("mac");
    private static final String FILE_SEPERATOR = System.getProperty("file.separator");

    public CreateQuickloadAction() {
        super("Save Custom Genome to Local Quickload Site", null, null, null, KeyEvent.VK_UNDEFINED, null, true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (IS_MAC) {
            showFileDialog(gmodel.getSelectedGenomeVersion());
        } else {
            showFileChooser(gmodel.getSelectedGenomeVersion());
        }
    }

    private void showFileDialog(GenomeVersion genomeVersion) {
        if (genomeVersion == null) {
            JOptionPane.showMessageDialog(service.getApplicationFrame(), "No custome genome to save", "IGB", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        System.setProperty("apple.awt.fileDialogForDirectories", "true");
        FileDialog fileDialog = new FileDialog(igbService.getApplicationFrame(), "Save Quickload", FileDialog.SAVE);
        fileDialog.setVisible(true);
        if (fileDialog.getFile() != null) {
            createQuickload(genomeVersion, new File(fileDialog.getDirectory(), fileDialog.getFile()));
        }
    }

    private void showFileChooser(GenomeVersion genomeVersion) {
        if (genomeVersion == null) {
            JOptionPane.showMessageDialog(service.getApplicationFrame(), "No custome genome to save", "IGB", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Save Quickload");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);

        if (chooser.showSaveDialog(igbService.getApplicationFrame()) == JFileChooser.APPROVE_OPTION) {
            createQuickload(genomeVersion, chooser.getSelectedFile());
        }
    }

    private void createQuickload(GenomeVersion genomeVersion, File selectedFolderPath) throws SecurityException {
        String[] quickloadFiles = {"contents.txt", "genome.txt", "annots.xml"};

        // Details of genome.txt
        StringBuilder genomeBuilder = new StringBuilder();
        for (int i = 0; i < genomeVersion.getSeqList().size(); i++) {
            genomeBuilder.append(genomeVersion.getSeqList().get(i).getId());
            genomeBuilder.append(PropertyConstants.TAB);
            genomeBuilder.append(genomeVersion.getSeqList().get(i).getMax());
            genomeBuilder.append(PropertyConstants.NEXT_LINE);
        }

        // Details of contents.txt
        String genomeName = StringUtils.isEmpty(genomeVersion.getName()) ? PropertyConstants.EMPTY : genomeVersion.getName();
        String genomeDescription = StringUtils.isEmpty(genomeVersion.getDescription()) ? genomeVersion.getSpeciesName() : genomeVersion.getDescription();

        Map<String, String> map = new HashMap<>(2);
        map.put(quickloadFiles[0], genomeName + PropertyConstants.TAB + genomeDescription);
        map.put(quickloadFiles[1], genomeBuilder.toString());

        String folderPath = selectedFolderPath.toString() + FILE_SEPERATOR;

        for (int i = 0; i < map.size(); i++) {
            if (i > 0) {
                folderPath += genomeName + FILE_SEPERATOR;
            }
            new File(folderPath).mkdir();
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(folderPath + quickloadFiles[i], false))) {
                writer.write(map.get(quickloadFiles[i]));
                writer.flush();
            } catch (IOException ex) {
                LOG.error(ex.getMessage(), ex);
            }
        }

        //Details of annots.xml
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        Document doc = null;
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.newDocument();

            Element rootElement = doc.createElement("files");
            doc.appendChild(rootElement);

            for (DataContainer dataContainer : genomeVersion.getDataContainers()) {
                for (DataSet dataSet : dataContainer.getDataSets()) {

                    Element file = doc.createElement("file");
                    rootElement.appendChild(file);

                    String uri = dataSet.getURI().toString();
                    file.setAttribute(PropertyConstants.PROP_NAME, uri);
                    file.setAttribute(PropertyConstants.PROP_TITLE, dataSet.getDataSetName());
                    file.setAttribute(PropertyConstants.PROP_REFERENCE, String.valueOf(dataSet.isReferenceSequence()));

                    ITrackStyleExtended iTrackStyleExtended = service.getAnnotStyle(uri);

                    if (dataSet.getProperties() != null || iTrackStyleExtended != null) {

                        Map<String, String> properties = dataSet.getProperties();

                        //foreground
                        String foregroundVal = PropertyConstants.PROP_FOREGROUND;
                        String foreground = (properties != null) ? properties.get(foregroundVal) : null;
                        Object iTrackForeground = (iTrackStyleExtended != null) ? iTrackStyleExtended.getForeground() : null;
                        setFileAttribute(foreground, foregroundVal, iTrackForeground, file);

                        //background
                        String backgroundVal = PropertyConstants.PROP_BACKGROUND;
                        String background = (properties != null) ? properties.get(backgroundVal) : null;
                        Object iTrackBackground = (iTrackStyleExtended != null) ? iTrackStyleExtended.getBackground() : null;
                        setFileAttribute(background, backgroundVal, iTrackBackground, file);

                        //positive strand color
                        String postiveStrandVal = PropertyConstants.PROP_POSITIVE_STRAND;
                        String positiveStrandColor = (properties != null) ? properties.get(postiveStrandVal) : null;
                        Object iTrackForwardColor = (iTrackStyleExtended != null) ? iTrackStyleExtended.getForwardColor() : null;
                        setFileAttribute(positiveStrandColor, postiveStrandVal, iTrackForwardColor, file);

                        //negative strand color
                        String negativeStrandVal = PropertyConstants.PROP_NEGATIVE_STRAND;
                        String negativeStrandColor = (properties != null) ? properties.get(negativeStrandVal) : null;
                        Object iTrackReverseColor = (iTrackStyleExtended != null) ? iTrackStyleExtended.getReverseColor() : null;
                        setFileAttribute(negativeStrandColor, negativeStrandVal, iTrackReverseColor, file);

                        //name size
                        String nameSizeVal = PropertyConstants.PROP_NAME_SIZE;
                        String nameSize = (properties != null) ? properties.get(nameSizeVal) : null;
                        Object iTrackNameSize = (iTrackStyleExtended != null) ? iTrackStyleExtended.getTrackNameSize() : null;
                        setFileAttribute(nameSize, nameSizeVal, iTrackNameSize, file);

                        //show2Tracks
                        String show2TrackVal = PropertyConstants.PROP_SHOW_2TRACK;
                        String show2Track = (properties != null) ? properties.get(show2TrackVal) : null;
                        Object iTrackSeperate = (iTrackStyleExtended != null) ? iTrackStyleExtended.getSeparate() : null;
                        setFileAttribute(show2Track, show2TrackVal, iTrackSeperate, file);

                        //connected
                        String connectedVal = PropertyConstants.PROP_CONNECTED;
                        String connected = (properties != null) ? properties.get(connectedVal) : null;
                        Object iTrackConnected = (iTrackStyleExtended != null) ? iTrackStyleExtended.getConnected() : null;
                        setFileAttribute(connected, connectedVal, iTrackConnected, file);

                        //label field
                        String labelFieldVal = PropertyConstants.PROP_LABEL_FIELD;
                        String labelField = (properties != null) ? properties.get(labelFieldVal) : null;
                        Object iTrackLabelField = (iTrackStyleExtended != null) ? iTrackStyleExtended.getLabelField() : null;
                        setFileAttribute(labelField, labelFieldVal, iTrackLabelField, file);

                        //max depth
                        String maxDepthVal = PropertyConstants.PROP_MAX_DEPTH;
                        String maxDepth = (properties != null) ? properties.get(maxDepthVal) : null;
                        Object iTrackMaxDepth = (iTrackStyleExtended != null) ? iTrackStyleExtended.getMaxDepth() : null;
                        setFileAttribute(maxDepth, maxDepthVal, iTrackMaxDepth, file);

                        //direction type
                        String directionTypeVal = PropertyConstants.PROP_DIRECTION_TYPE;
                        String directionType = (properties != null) ? properties.get(directionTypeVal) : null;
                        Object iTrackDirectionType = (iTrackStyleExtended != null) ? PropertyConstants.DirectionType.values()[iTrackStyleExtended.getDirectionType()] : null;
                        setFileAttribute(directionType, directionTypeVal, iTrackDirectionType, file);

                        // index
                        if (dataSet.getIndex().isPresent()) {
                            file.setAttribute(PropertyConstants.PROP_INDEX, dataSet.getIndex().get().toString());
                        }

                        //description
                        String descriptionVal = PropertyConstants.PROP_DESCRIPTION;
                        String description = (properties != null) ? properties.get(descriptionVal) : null;
                        setFileAttribute(description, descriptionVal, null, file);

                        //url
                        String urlVal = PropertyConstants.PROP_URL;
                        String url = (properties != null) ? properties.get(urlVal) : null;
                        Object iTrackUrl = (iTrackStyleExtended != null) ? iTrackStyleExtended.getUrl() : null;
                        setFileAttribute(url, urlVal, iTrackUrl, file);

                        //load hint
                        String loadHint = dataSet.getLoadStrategy().name();
                        if (loadHint != null && loadHint.equals(PropertyConstants.WHOLE_GENOME)) {
                            file.setAttribute(PropertyConstants.LOAD_HINT, loadHint);
                        }
                    }
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(folderPath + quickloadFiles[quickloadFiles.length - 1]);
            transformer.transform(source, result);

        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    private void setFileAttribute(String propertyVal, String propertyName, Object trackStyle, Element file) throws DOMException {
        if ((propertyVal != null && trackStyle != null) || trackStyle != null) {
            String value = (trackStyle instanceof Color) ? PreferenceUtils.getColorString((Color) trackStyle) : trackStyle.toString();
            file.setAttribute(propertyName, value);
        } else if (propertyVal != null) {
            file.setAttribute(propertyName, propertyVal);
        }
    }

    @Reference(optional = false)
    public void setIgbService(IgbService igbService) {
        this.service = igbService;
    }

    @Override
    public Optional<List<MenuItem>> getMenuItems() {
        MenuItem saveCustomGenomeMenuItem = new MenuItem("Save Custom Genome to Local Quickload Site", (Void t) -> {
            actionPerformed(null);
            return t;
        });
        try (InputStream resourceAsStream = CreateQuickloadAction.class.getClassLoader().getResourceAsStream("saveCustomGenome.png")) {
            saveCustomGenomeMenuItem.setMenuIcon(new MenuIcon(resourceAsStream));
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }

        saveCustomGenomeMenuItem.setWeight(MENU_POSITION);
        return Optional.of(Arrays.asList(saveCustomGenomeMenuItem));
    }

    @Override
    public MenuBarParentMenu getMenuExtensionParent() {
        return MenuBarParentMenu.FILE;
    }

}
